
create table user(
    id int primary key AUTO_INCREMENT,
    name varchar(50),
    contactNumber varchar(50),
    email varchar(50),
    password varchar(50),
    status varchar(50),
    role varchar(50),
    unique (email)
);

insert into user(name, contactNumber, email, password, status, role) values('Pablito','0990423854', 'p@gmail.com','1234','true','admin');
insert into user(name, contactNumber, email, password, status, role) values('Berniece Heathcote','0990423854', 'berniece.heathcote6@ethereal.email','YG1w6jFR4KDB1bQmQA','true','user');

create table category(
    id int not null AUTO_INCREMENT,
    name varchar(250) not null,
    primary key(id)
);


create table product(
    id int not null AUTO_INCREMENT,
    name varchar(255) not null,
    categoryId integer not null,
    description varchar(250),
    price integer,
    status varchar(20),
    primary key(id)
);


create table bill(
    id int not null AUTO_INCREMENT,
    uuid varchar(200) not null,
    name varchar(255) not null,
    email varchar(255) not null,
    contactNumber varchar(20) not null,
    paymentMethod varchar(50) not null,
    total int not null,
    productDetails JSON default null,
    createdBy varchar(255) not null,
    primary key(id)
);
